//
//  HomeViewController.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: UIViewController {
    private(set) var viewModel: HomeViewModel!
    fileprivate var router: HomeRouter!
    fileprivate let disposeBag = DisposeBag()
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var items = [TypiData]()

    init(withViewModel viewModel: HomeViewModel, router: HomeRouter) {
        self.viewModel = viewModel
        self.router = router
        super.init(nibName: "Home", bundle: nil)
    }

    func load(withViewModel viewModel: HomeViewModel, router: HomeRouter) {
        self.viewModel = viewModel
        self.router = router
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupLayout()
        setupRx()
    }
}

// MARK: Setup
private extension HomeViewController {

    func setupViews() {
        let addButton = UIBarButtonItem(title: "Add Contact", style: .done, target: self, action: #selector(addContact))
        self.navigationItem.rightBarButtonItem  = addButton
    }

    func setupLayout() {
    
    }

    func setupRx() {
    }
    
    @objc func addContact(){
        self.router.navigateToAddContact()
    }
}
