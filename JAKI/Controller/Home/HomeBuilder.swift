//
//  HomeBuilder.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

struct HomeBuilder {
    
    static func viewController() -> UIViewController {
        let viewModel = HomeViewModel()
        let router = HomeRouter()
        let viewController = HomeViewController(withViewModel: viewModel, router: router)
        router.viewController = viewController
        return viewController
    }
    
    static func boardViewController() -> UIViewController {
        let viewModel = HomeViewModel()
        let router = HomeRouter()
        let viewController = HomeViewController(nibName: "Home", bundle: Bundle.main)
        viewController.load(withViewModel: viewModel, router: router)
        router.viewController = viewController
        return viewController
    }
}
