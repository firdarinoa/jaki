//
//  HomeRouter.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import Foundation

class HomeRouter {
    weak var viewController: HomeViewController?

    func navigateToAddContact() {
        let addContactViewController  = AddContactBuilder.viewController() as! AddContactViewController
        viewController?.navigationController?.pushViewController(addContactViewController, animated: true)
    }
    
    func navigateToDetailContact() {
        let detailContactViewController  = DetailContactBuilder.viewController() as! DetailContactViewController
        viewController?.navigationController?.pushViewController(detailContactViewController, animated: true)
    }
}
