//
//  DetailContactBuilder.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

struct DetailContactBuilder {
    
    static func viewController() -> UIViewController {
        let viewModel = DetailContactViewModel()
        let router = DetailContactRouter()
        let viewController = DetailContactViewController(withViewModel: viewModel, router: router)
        router.viewController = viewController
        
        return viewController
    }
    
    static func boardViewController() -> UIViewController {
        let viewModel = DetailContactViewModel()
        let router = DetailContactRouter()
        
        let viewController = DetailContactViewController(nibName: "DetailContact", bundle: Bundle.main)
        viewController.load(withViewModel: viewModel, router: router)
        
        
        router.viewController = viewController
        
        return viewController
    }
}
