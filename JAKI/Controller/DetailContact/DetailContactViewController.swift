//
//  DetailContactViewController.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class DetailContactViewController: UIViewController {
    private(set) var viewModel: DetailContactViewModel!
    fileprivate var router: DetailContactRouter!
    fileprivate let disposeBag = DisposeBag()

    init(withViewModel viewModel: DetailContactViewModel, router: DetailContactRouter) {
        self.viewModel = viewModel
        self.router = router
        super.init(nibName: "DetailContact", bundle: nil)
    }

    func load(withViewModel viewModel: DetailContactViewModel, router: DetailContactRouter) {
        self.viewModel = viewModel
        self.router = router
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupLayout()
        setupRx()
    }
}

// MARK: Setup
private extension DetailContactViewController {

    func setupViews() {
        
    }

    func setupLayout() {
    
    }

    func setupRx() {
    
    }
}
