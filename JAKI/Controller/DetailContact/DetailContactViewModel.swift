//
//  DetailContactViewModel.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import RxSwift
import RxCocoa

class DetailContactViewModel {
    
    // input
    
    // output
    
    // internal
    fileprivate let disposeBag =  DisposeBag()
    init() {
        setupRx()
    }
}

// MARK: Setup
private extension DetailContactViewModel {
    
    func setupRx() {
        
    }
}
