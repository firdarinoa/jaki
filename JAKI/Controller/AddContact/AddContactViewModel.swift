//
//  AddContactViewModel.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import RxSwift
import RxCocoa

class AddContactViewModel {
    
    let items: BehaviorRelay<[TypiData]>  = BehaviorRelay<[TypiData]>(value:[])
    let success:BehaviorRelay<ShieldResponse?> = BehaviorRelay<ShieldResponse?>(value:nil)
    let error:BehaviorRelay<ShieldError?> = BehaviorRelay<ShieldError?>(value: nil)
    fileprivate let disposeBag =  DisposeBag()
    
    init() {
        setupRx()
    }
    
    func fetchData() {
        guard let url = URL(string: Constants.url) else {return}
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                let decoder = JSONDecoder()
                let response = try decoder.decode([TypiData].self, from: data!)
                self.items.accept(response)
                let success = ShieldResponse()
                success.result  = data
                self.success.accept(success)
            } catch let parsingError {
                print(parsingError)
                let responseError = ShieldError()
                self.error.accept(responseError)
            }
        }
        task.resume()
    }
}

// MARK: Setup
private extension AddContactViewModel {
    
    func setupRx() {
    }
}
