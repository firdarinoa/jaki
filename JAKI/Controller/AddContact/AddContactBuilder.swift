//
//  AddContactBuilder.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit

struct AddContactBuilder {
    
    static func viewController() -> UIViewController {
        let viewModel = AddContactViewModel()
        let router = AddContactRouter()
        let viewController = AddContactViewController(withViewModel: viewModel, router: router)
        router.viewController = viewController
        
        return viewController
    }
    
    static func boardViewController() -> UIViewController {
        let viewModel = AddContactViewModel()
        let router = AddContactRouter()
        
        let viewController = AddContactViewController(nibName: "AddContact", bundle: Bundle.main)
        viewController.load(withViewModel: viewModel, router: router)
        
        
        router.viewController = viewController
        
        return viewController
    }
}
