//
//  AddContactViewController.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//  Copyright (c) 2021 ___ORGANIZATIONNAME___. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AddContactViewController: UIViewController {
    private(set) var viewModel: AddContactViewModel!
    fileprivate var router: AddContactRouter!
    fileprivate let disposeBag = DisposeBag()
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var items = [TypiData]()
    
    init(withViewModel viewModel: AddContactViewModel, router: AddContactRouter) {
        self.viewModel = viewModel
        self.router = router
        super.init(nibName: "AddContact", bundle: nil)
    }

    func load(withViewModel viewModel: AddContactViewModel, router: AddContactRouter) {
        self.viewModel = viewModel
        self.router = router
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
        setupLayout()
        setupRx()
    }
}

// MARK: Setup
private extension AddContactViewController {

    func setupViews() {
        viewModel.fetchData()
        collectionView.delegate = self
        collectionView.register(UINib.init(nibName: "HomeCell", bundle: nil), forCellWithReuseIdentifier: "HomeCell")
    }

    func setupLayout() {
    
    }

    func setupRx() {
        self.viewModel.items.asObservable().bind(to: self.collectionView.rx.items(cellIdentifier: "ContactCell", cellType: ContactCell.self)) { (row, element, cell) in
            cell.nameLabel.text = element.results[row].name.first
        }
        .disposed(by: self.disposeBag)
        
        self.viewModel.success.subscribe(onNext: { (any) in
            guard let result = any?.result  as? Data else{
                return
            }
        }).disposed(by: self.disposeBag)

        self.viewModel.error.subscribe(onNext: { (response) in
            guard response != nil else {
                return
            }
        }).disposed(by: self.disposeBag)
    }
    
}


extension AddContactViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: 95)
    }
}
