//
//  ViewController.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func beginButtonTapped(_ sender: Any) {
        let homeViewController  = HomeBuilder.viewController() as! HomeViewController
        self.navigationController?.pushViewController(homeViewController, animated: true)
    }
}
