//
//  Post.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//

import Foundation

struct TypiData: Codable {
    var results: [Results]
}

struct Results: Codable {
    var gender: String
    var name: Name
    var location: Location
}

struct Name: Codable {
    var title: String
    var first: String
    var last: String
}

struct Location: Codable {
    var street: Street
    var city: String
    var state: String
    var country: String
    var postcode: Int
    var coordinates: Coordinates
    var timezone: Timezone
    var email: String
    var dob: Dob
    var phone: String
    var cell: String
    var id: Id
    var picture: Picture
    var nat: String
}

struct Street: Codable {
    var number: Int
    var name: String
}

struct Coordinates: Codable {
    var latitude: String
    var longitude: String
}

struct Timezone: Codable {
    var offset: String
    var description: String
}

struct Dob: Codable {
    var date: String
    var age: Int
}

struct Id: Codable {
    var name: String
    var value: String
}

struct Picture: Codable {
    var large: String
    var medium: String
    var thumbnail: String
}
