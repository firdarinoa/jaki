//
//  Constants.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//

import Foundation

struct Constants {
    static let url = "https://randomuser.me/api?results=5&exc=login,registered,id,nat&nat=us&noinfo"
}
