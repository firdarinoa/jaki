//
//  ContactCell.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//

import UIKit

class ContactCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
