//
//  NetworkAPI.swift
//  JAKI
//
//  Created by Firda Sahidi on 26/01/21.
//

import Foundation

class ShieldResponse {
    var response:HTTPURLResponse!
    var result:Any!
    var id:String!
}
class ShieldError {
    
    var requestTimeout: (() -> (Void))?
    var id:String!
    var wantShowError:Bool = false
    var error:Error!{
        willSet{
            if newValue != nil{
                if newValue._code == NSURLErrorTimedOut {
                    if let request = requestTimeout{
                        request()
                    }
                    
                }
            }
        }
    }
}
